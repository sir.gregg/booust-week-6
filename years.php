<!DOCTYPE html>
<html>
	<head>
		<title>Years Between 1980 - 2018</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body class="php-style">
		<div class="php-style-container">
			<h1 class="head-style">Years Between 1980 - 2018</h1>
			<div class="container php-container">
				<?php
				  $a = 2018;
				 
				  for( $b = 1980; $b<=$a; $b++ ) {
				     if ($b % 4) {
				         echo "$b </br>";
				     } else {
				         echo "$b is a leap year</br>";
				     }
				  }
				?>

				<a href="index.html" class="btn-year">Go to Home Page</a>
			</div>
		</div>
	</body>
</html>


